# burnedUBI

This project tracks the burning of UBI token (Universal Basic Income). To do so, it creates a json with ALL the transactions that burned UBI, that is via burning directly by sending them to a null address or by using ubiburner.

On top of this, there's another script to update the burned UBI on a notion database, this way, the whole community can see which project contributes and how much UBI is being burned.

## How to use 

### Generate json

If you want to get the json with all the transactions, just run the script with your etherscan api key in plain text in a file called 'apikey' on the same directory as the script. This will generate a json with the desired data, it's structure is the next one:

![image](/uploads/2c1837603247b8adf1befdb7239295d5/image.png)

### Update notion database

To update the notion database, you need admin rights to that workspace. But don't worry, admins are running this script frequently so it's updated. If you want to create your own database, you'll need a notion api token in a file called 'notion_token' on the same directory as the script. You'll also need a database with the same structure and that databaseId. For more info, refer to the notion api documentation.

___

Any tips, contributions and questions are welcome
