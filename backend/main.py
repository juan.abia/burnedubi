import burned_ubi
import utils
import os
import sys

# Change working directory to path of this script
os.chdir(os.path.dirname(sys.argv[0]))

bu = burned_ubi.burnedUbi()
data = utils.Data()
format_bu = burned_ubi.format_burned_ubi()
ns = utils.ens()

print("Updating aliases")
ns.update_aliases()

print("Getting burned ubi")
data.dict_to_json(bu.get_burned_ubi(), "burned_ubi")
data.dict_to_json(format_bu.sort_by_date(), "burned_ubi_by_date")