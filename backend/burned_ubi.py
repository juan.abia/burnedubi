import utils
import datetime
import copy

class burnedUbi:
    def __init__(self):
        self.data = utils.Data()
        self.aliases = self.data.get_aliases()
        self.etherscan = utils.etherscan(self.data.get_etherscan_api_key())

        self.posta_contract_address="0xaE199eB85A303D11725D193Efd1E6aB312a980B6"
        self.ubi_burner_address="0x481B24Ed5feAcB37e282729b9815e27529Cf9ae2"
        self.null_addresses=["0x000000000000000000000000000000000000dead","0xdEAD000000000000000042069420694206942069", "0x0000000000000000000000000000000000000001",
            "0x0000000000000000000000000000000000000002", "0x0000000000000000000000000000000000000003", "0x0000000000000000000000000000000000000004", 
            "0x0000000000000000000000000000000000000005", "0x0000000000000000000000000000000000000006", "0x0000000000000000000000000000000000000007", 
            "0x0000000000000000000000000000000000000008", "0x0000000000000000000000000000000000000009", "0x00000000000000000000045261d4ee77acdb3286", 
            "0x0000000000000000000000000000000000000000", "0x0123456789012345678901234567890123456789", "0x1111111111111111111111111111111111111111", 
            "0x1234567890123456789012345678901234567890", "0x2222222222222222222222222222222222222222", "0x3333333333333333333333333333333333333333", 
            "0x4444444444444444444444444444444444444444", "0x6666666666666666666666666666666666666666", "0x8888888888888888888888888888888888888888", 
            "0xbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb", "0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", "0xffffffffffffffffffffffffffffffffffffffff", 
            "0xaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"]

    # UBI burn burner is every address that has realized an ETH transaction to the ubiburner contract
    def ubi_burner_burners(self):
        """
            aliases: (Dict) aliases from eth addresses
            apikey: etherscan apikey
        """
        ubi_burner_burners = {}
        ubi_burner_address="0x481B24Ed5feAcB37e282729b9815e27529Cf9ae2"
        txs = self.etherscan.eth_transactions(ubi_burner_address, False)
        for tx in txs:
            if not tx["from"] in ubi_burner_burners:
                ubi_burner_burners[tx["from"]] = {"alias": "", "total": 0, "transactions": {}}
            
            ubi_burner_burners[tx["from"]]["transactions"][tx["hash"]] = {
                                                            "date": datetime.datetime.fromtimestamp(int(tx["timeStamp"])).strftime('%Y-%m-%d %H:%M:%S'),
                                                            "eth": int(tx["value"]) / 1E18
                                                            }
            ubi_burner_burners[tx["from"]]["total"] += int(tx["value"]) / 1E18

            # If burner addres is in the alias list, asign it. If not, set the address as the alias
            if tx["from"] in self.aliases:
                ubi_burner_burners[tx["from"]]["alias"] = self.aliases[tx["from"]]
            else:
                ubi_burner_burners[tx["from"]]["alias"] = tx["from"]

        return ubi_burner_burners

    def ubi_burner(self):
        ubi_burner_json={"address": self.ubi_burner_address,
                        "burned_ubi": self.etherscan.ubi_balance(self.ubi_burner_address),
                        "current_eth": self.etherscan.ether_balance(self.ubi_burner_address),
                        "burners": self.ubi_burner_burners()}

        return ubi_burner_json

    # Direct burn burner is every address that has realized an UBI transaction to a null address
    def direct_burn_burners(self):
        """
        Get UBI transsactions for every null address, and create a dict containing the important information from each transaction
        """
        direct_burn_burners = {}
        
        for null_address in self.null_addresses:    
            txs = self.etherscan.ubi_transactions(null_address, False)
            for tx in txs:
                if not tx["from"] in direct_burn_burners:
                    direct_burn_burners[tx["from"]] = {"alias": "", "total": 0, "transactions": {}}
                
                direct_burn_burners[tx["from"]]["transactions"][tx["hash"]] = {
                                                                "date": datetime.datetime.fromtimestamp(int(tx["timeStamp"])).strftime('%Y-%m-%d %H:%M:%S'),
                                                                "ubi": int(tx["value"]) / 1E18
                                                                }
                direct_burn_burners[tx["from"]]["total"] += int(tx["value"]) / 1E18
        
                # If burner addres is in the alias list, asign it. If not, set the address as the alias
                if tx["from"] in self.aliases:
                        direct_burn_burners[tx["from"]]["alias"] = self.aliases[tx["from"]]
                else:
                    direct_burn_burners[tx["from"]]["alias"] = tx["from"]

        return direct_burn_burners

    def direct_burn(self):
        """
        Calculate the ammount of total burned UBI
        """
        direct_burn_json={"burned_ubi": 0, "burners": self.direct_burn_burners()}
        for burner in direct_burn_json["burners"]:
            direct_burn_json["burned_ubi"] += direct_burn_json["burners"][burner]["total"]
        
        return direct_burn_json

    # UBI burned through posta will be assigned to an individual address not posta contract
    # the next functions will reassing this burn to posta contract
    def get_posta_txs_hashes(self):
        """
        get hashes of transactions that burn UBI through posta
        """
        posta_txs = self.etherscan.eth_normal_transactions(self.posta_contract_address, False)
        
        posta_txs_hashes=[]
        for tx in posta_txs:
            posta_txs_hashes.append(tx["hash"])

        return posta_txs_hashes

    def reassign_posta_burns(self, burned_ubi):
        initial_ubi = burned_ubi["direct_burn"]["burned_ubi"]

        posta_txs_hashes = self.get_posta_txs_hashes()
        
        burners = copy.deepcopy(burned_ubi["direct_burn"]["burners"])
        burners[self.posta_contract_address] = {"alias": "posta", "total": 0, "transactions": {}}

        for burner in burned_ubi["direct_burn"]["burners"]:
            for tx_hash in burned_ubi["direct_burn"]["burners"][burner]["transactions"]:
                if tx_hash in posta_txs_hashes:
                    burners[self.posta_contract_address]["transactions"][tx_hash] = burners[burner]["transactions"][tx_hash] 
                    burners[self.posta_contract_address]["total"] += burners[burner]["transactions"][tx_hash]["ubi"]              

                    # If that burner only has 1 tx, remove it 
                    if len(burners[burner]["transactions"]) <= 1:
                        del burners[burner]

                    # If not, substract the burned UBI from the total
                    else:
                        burners[burner]["total"] -= burners[burner]["transactions"][tx_hash]["ubi"]
                        del burners[burner]["transactions"][tx_hash]

        burned_ubi["direct_burn"]["burners"] = copy.deepcopy(burners)
        
        final_ubi = [burned_ubi["direct_burn"]["burners"][burner]["total"] for burner in burned_ubi["direct_burn"]["burners"]]
        final_ubi = sum(final_ubi)
        
        if final_ubi != initial_ubi:
            print("ERROR: reassigned_posta_burns changed the ammount of total burned UBI")
            exit()

        return burned_ubi
    
    def get_burned_ubi(self):
        burned_ubi_raw = {"direct_burn": self.direct_burn(), "ubi_burner": self.ubi_burner()}
        burned_ubi = self.reassign_posta_burns(burned_ubi_raw)
        return burned_ubi

class format_burned_ubi:
    def __init__(self):
        self.data = utils.Data()
        self.burned_ubi = self.data.json_to_dict("burned_ubi")
        self.burned_ubi_list = self.get_ubi_burns_list()

    def get_ubi_burns_list(self):
        """
        Get a list of UBI burns transactions from burned_ubi dict
        """
        burned_ubi_list = []

        for burner in self.burned_ubi["direct_burn"]["burners"]:
            for tx in self.burned_ubi["direct_burn"]["burners"][burner]["transactions"]:
                date = self.burned_ubi["direct_burn"]["burners"][burner]["transactions"][tx]["date"].split(" ")[0]
                ubi = self.burned_ubi["direct_burn"]["burners"][burner]["transactions"][tx]["ubi"]
                alias = self.burned_ubi["direct_burn"]["burners"][burner]["alias"]
                burned_ubi_list.append([date,ubi,alias,tx])

        return burned_ubi_list

    def sort_by_date(self):    
        first_tx_date = datetime.date(2021, 3, 20)
        today_date = datetime.datetime.today().date()
        dates = [(first_tx_date+datetime.timedelta(days=x)).strftime('%Y-%m-%d') for x in range((today_date-first_tx_date).days)]
        
        burned_ubi_by_date = {}

        for date in dates:
            burned_ubi_by_date[date] = {}

            for tx in self.burned_ubi_list:
                if tx[0] == date:
                    if tx[2] not in burned_ubi_by_date[date]:
                        burned_ubi_by_date[date][tx[2]] = {}
                    burned_ubi_by_date[date][tx[2]][tx[3]] = {"amount": tx[1]}

        return burned_ubi_by_date