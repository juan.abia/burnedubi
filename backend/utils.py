from audioop import add
import requests
import json
import os
import secrets
from web3 import Web3
from ens import ENS


class etherscan:
    def __init__(self, apikey):
        self.apikey = apikey
        self.ubi_token_address = "0xdd1ad9a21ce722c151a836373babe42c868ce9a4"

    def ether_balance(self, address):
        """
        Get ether balance of an address
        """
        address = address.lower()
        url = f"https://api.etherscan.io/api?module=account&action=balance&address={address}&tag=latest&apikey={self.apikey}"
        response = requests.get(url)
        return int(response.json()["result"]) / 1E18

    def ubi_balance(self, address):
        """
        Get UBI balance of an address
        """
        address = address.lower()
        url = f"https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress={self.ubi_token_address}&address={address}&tag=latest&apikey={self.apikey}"
        response = requests.get(url)
        return int(response.json()["result"]) / 1E18

    def eth_normal_transactions(self, address, out):
        """
        Get a list of normal transactions in or out of an address

        - 'out' should be True when you want the list of transaction FROM the provided address
           False if you want transactions TO the provided address 
        """
        page = 1
        address = address.lower()
        txs_list = []
        txs = [0]

        while len(txs) > 0:
            url = f"https://api.etherscan.io/api?module=account&action=txlist&address={address}&startblock=0&endblock=999999999&page={page}&offset=10&sort=asc&apikey={self.apikey}"
            response = requests.get(url)
            txs = response.json()["result"]
            for tx in txs:
                if (tx["to"] == address) and (not out) and (tx["isError"] != "1"):
                    txs_list.append(tx)

            page += 1

        return txs_list

    def eth_internal_transactions(self, address, out):
        """
        Get a list of internal transactions in or out of an address

        - 'out' should be True when you want the list of transaction FROM the provided address
           False if you want transactions TO the provided address 
        """
        page = 1
        address = address.lower()
        txs_list = []
        txs = [0]

        while len(txs) > 0:
            url = f"https://api.etherscan.io/api?module=account&action=txlistinternal&address={address}&startblock=0&endblock=999999999&page={page}&offset=10&sort=asc&apikey={self.apikey}"
            response = requests.get(url)
            txs = response.json()["result"]
            for tx in txs:
                if (tx["to"] == address) and (not out) and (tx["isError"] != "1"):
                    txs_list.append(tx)

            page += 1

        return txs_list

    def eth_transactions(self, address, out):
        """
        Get a list of both normal and internal transactions
        """
        normal = self.eth_normal_transactions(address, out)
        internal = self.eth_internal_transactions(address, out)
        return normal + internal

    def ubi_transactions(self, address, out):
        """
        Get a list of UBI transactions in or out of an address
        """
        page = 1
        address = address.lower()
        ubi_token_address = "0xdd1ad9a21ce722c151a836373babe42c868ce9a4"
        txs_list = []
        txs = [0]

        while len(txs) > 0:
            url = f"https://api.etherscan.io/api?module=account&action=tokentx&contractaddress={ubi_token_address}&address={address}&page={page}&offset=100&startblock=0&endblock=999999999&sort=asc&apikey={self.apikey}"
            response = requests.get(url)
            txs = response.json()["result"]
            for tx in txs:
                if (tx["to"] == address) and (not out):
                    txs_list.append(tx)

            page += 1

        return txs_list


class Data:
    def json_to_dict(self, name):
        with open(f"data/{name}.json") as json_file:
            file = json.load(json_file)
        return file

    def dict_to_json(self, dictionary, name):
        with open(f"data/{name}.json", "w+") as f:
            json.dump(dictionary, f)

    def get_etherscan_api_key(self):
        return secrets.etherscan_api_key

    def get_notion_token(self):
        return secrets.notion_token

    def get_aliases(self):
        aliases = self.json_to_dict("aliases")
        aliases = {k.lower(): v for k, v in aliases.items()}
        return aliases

    def get_all_burners_addresses(self):
        all_addresses = []
        burned_ubi = self.json_to_dict("burned_ubi")
        burners = burned_ubi["direct_burn"]["burners"] | burned_ubi["ubi_burner"]["burners"]
        return [address for address in burners]


class ens:
    def setup_node_connection(self):
        os.environ["WEB3_PROVIDER_URI"] = f"https://mainnet.infura.io/v3/{secrets.infura_project_id}"
        self.w3 = Web3()
        assert self.w3.isConnected()
        self.ns = ENS.fromWeb3(self.w3)

    def update_aliases(self):
        self.setup_node_connection()
        data = Data()
        aliases = data.json_to_dict("aliases")
        all_addresses = data.get_all_burners_addresses()
        all_aliases = self.get_aliases(all_addresses)
        for address in all_aliases:
            aliases[address] = all_aliases[address]
        
        data.dict_to_json(aliases, "aliases")

    def get_aliases(self, addresses):
        aliases = {}
        for address in addresses:
            alias = self.ns.name(address)
            if alias:
                aliases[address.lower()] = alias
        return aliases

ENS2 = ens()
ENS2.setup_node_connection()
ENS2.update_aliases()
