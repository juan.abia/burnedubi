#!/usr/bin/env python3

import requests, json

def readDatabase(databaseId, headers):
    readUrl = f"https://api.notion.com/v1/databases/{databaseId}/query"

    res = requests.request("POST", readUrl, headers=headers)
    data = res.json()
    return res.json()

def updatePageText(padeId, propertie, content, headers):
    updateUrl = f"https://api.notion.com/v1/pages/{pageId}"

    updateData = {"properties": {propertie: {"rich_text": [{"text": {"content": content}}]}}}
    data = json.dumps(updateData)

    response = requests.request("PATCH", updateUrl, headers=headers, data=data)
    return response.json()

def updatePageNumber(pageId, propertie, value, headers):
    updateUrl = f"https://api.notion.com/v1/pages/{pageId}"

    updateData = {"properties":{propertie: {"number": value}}}
    data = json.dumps(updateData)

    response = requests.request("PATCH", updateUrl, headers=headers, data=data)
    return response.json()

def loadBurnedUbiJson():
    with open('burned_ubi.json') as json_file:
        burned_ubi = json.load(json_file)
        return burned_ubi

def totalBurnedList():
    burned_ubi = loadBurnedUbiJson()
    direct_burners = {}
    ubiburner_burners = {}
    
    # Direct burn
    for burner in burned_ubi["direct_burn"]["burners"]:
        direct_burners[burner.lower()] = burned_ubi["direct_burn"]["burners"][burner]["total"]

    # ubiburner (donated ETH to burn UBI)
    for burner in burned_ubi["ubi_burner"]["burners"]:
        ubiburner_burners[burner.lower()] = burned_ubi["ubi_burner"]["burners"][burner]["total"]

    return direct_burners, ubiburner_burners

# -------------------------------------------

token = ""
with open("notion_token", 'r') as f:
    token = f.read().strip()

databaseId = "b51fe8467efa45bab18925ad1e14bd6b"

headers = {
    "Authorization": "Bearer " + token,
    "Content-Type": "application/json",
    "Notion-Version": "2021-08-16"
}


direct_burners, ubiburner_burners = totalBurnedList()

for page in readDatabase(databaseId, headers)["results"]:
    # check if page has an address
    if len(page["properties"]["Address"]["rich_text"]) > 0:
        
        # The same proyect can have multiple addresses, separate the by a comma
        addresses = page["properties"]["Address"]["rich_text"][0]["text"]["content"].lower().split(",")
        
        # 'burned' list contains the number of burned ubi ('0') and donated eth ('1')
        burned = ([0],[0])
        for address in addresses:
            if address in direct_burners:
                burned[0].append(direct_burners[address])
            if address in ubiburner_burners:
                burned[1].append(ubiburner_burners[address])

        updatePageNumber(page["id"], "burned UBI", round(sum(burned[0]),2), headers)
        updatePageNumber(page["id"], "donated ETH (UBIBurner)", round(sum(burned[1]),6), headers)
